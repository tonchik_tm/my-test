<?php
/**
 *
 */

namespace TonchikTm\MyTest;

/**
 * Interface IGenerator
 * @package MyTest\InterfaceGenerator
 */
interface IGenerator
{
    /**
     * @param string $filename
     */
    public function parse(string $filename);

    /**
     * @return string
     */
    public function output() : string;
}