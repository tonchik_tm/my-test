<?php
/**
 *
 */

namespace TonchikTm\MyTest;


class ServiceUpload implements IServiceUpload
{
    private $filename = null;

    /**
     * @param string $fileField
     * @return bool
     */
    public function upload(string $fileField): bool
    {
        if (isset($_FILES[$fileField]) && is_array($_FILES[$fileField]) && $_FILES[$fileField]["tmp_name"] != "none") {
            $tmpName = $_FILES[$fileField]["tmp_name"];
            $oriName = $_FILES[$fileField]["name"];
        } else {
            return false;
        }

        if (is_uploaded_file($tmpName)) {
            $this->filename = $tmpName;
            return true;
        }
        return false;
    }

    public function save(string $filename): bool
    {
        return false;
    }

    public function getFile(): string
    {
        return $this->filename;
    }

    public function getFileContent(): string
    {
        return file_get_contents($this->filename);
    }

}