<?php
/**
 *
 */

namespace TonchikTm\MyTest;

/**
 * Interface IServiceUpload
 * @package MyTest\InterfaceGenerator
 */
interface IServiceUpload
{
    /**
     * @param string $fileField
     * @return bool
     */
    public function upload(string $fileField) : bool;

    /**
     * @param string $filename
     * @return bool
     */
    public function save(string $filename) : bool;

    /**
     * @return string
     */
    public function getFile() : string;

}