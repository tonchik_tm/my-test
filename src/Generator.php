<?php
/**
 *
 */

namespace TonchikTm\MyTest;

/**
 * Class Generator
 * @package MyTest\InterfaceGenerator
 *
 * @property \ReflectionClass $reflection
 * @property InterfaceData $data
 */
class Generator implements IGenerator
{
    private $reflection = null;
    private $data = null;

    /**
     * Generator constructor.
     * @param string $filename
     * @throws \Exception
     */
    public function __construct(string $filename)
    {
        $this->parse($filename);
    }

    /**
     * @param string $filename
     * @throws \Exception
     */
    public function parse(string $filename)
    {
        if ($this->createReflection($filename)) {

            $this->data = $this->createInterfaceData();

        } else throw new \Exception('The file doesn\'t contain classes!');
    }

    /**
     * @return string
     * @throws \ReflectionException
     */
    public function output(): string
    {
        return $this->data ? $this->data->generate() : null;
    }

    /**
     * @param string $filename
     * @return bool
     */
    private function createReflection(string $filename)
    {
        if (!is_file($filename)) return false;

        $defaultClasses = get_declared_classes();

        include_once($filename);
        $classes = array_values(array_diff_key(get_declared_classes(), $defaultClasses));

        if (count($classes) == 0) return false;

        $class = end($classes);
        try {
            $this->reflection = new \ReflectionClass($class);
        } catch (\ReflectionException $e) {
            return false;
        }

        return true;
    }

    /**
     * @return InterfaceData
     * @throws \ReflectionException
     */
    private function createInterfaceData()
    {
        $id = new InterfaceData($this->reflection->getShortName());

        if ($this->reflection->getNamespaceName())
            $id->setNamespace($this->reflection->getNamespaceName());

        $interfaceMethods = $this->getInterfaceMethods();
        $traitsMethods = $this->getTraitMethods();

        foreach ($this->reflection->getMethods() as $method) {
            $refMethod = new \ReflectionMethod($method->class, $method->name);

            if (
                !$refMethod->isPrivate() &&
                !$refMethod->isProtected() &&
                !in_array($method->name, $interfaceMethods) &&
                !in_array($method->name, $traitsMethods)
            ) {
                $id->addMethod($refMethod);
            }
        }

        return $id;
    }

    /**
     * @return array
     */
    private function getInterfaceMethods()
    {
        $interfaceMethods = array();
        foreach ($this->reflection->getInterfaces() as $interface) {
            foreach ($interface->getMethods() as $method) {
                $interfaceMethods[] = $method->name;
            }
        }
        return $interfaceMethods;
    }

    /**
     * @return array
     */
    private function getTraitMethods()
    {
        $traitsMethods = array();
        foreach ($this->reflection->getTraits() as $trait) {
            foreach ($trait->getMethods() as $method) {
                $traitsMethods[] = $method->name;
            }
        }
        return $traitsMethods;
    }

}