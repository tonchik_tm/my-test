<?php
/**
 *
 */

namespace TonchikTm\MyTest;

/**
 * Class InterfaceData
 * @package MyTest\InterfaceGenerator
 *
 * @property string $name
 * @property string $namespace
 * @property \ReflectionMethod[] $methods
 */
class InterfaceData
{
    private $name = null;
    private $namespace = null;
    private $methods = array();

    /**
     * InterfaceData constructor.
     * @param string $name
     */
    public function __construct($name)
    {
        $this->setName($name . 'Interface');
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getNamespace()
    {
        return $this->namespace;
    }

    /**
     * @param string $namespace
     */
    public function setNamespace($namespace)
    {
        $this->namespace = $namespace;
    }

    /**
     * @param \ReflectionMethod $method
     * @return bool
     */
    public function addMethod(\ReflectionMethod $method)
    {
        if (in_array($method, $this->methods)) return false;

        $this->methods[] = $method;

        return true;
    }

    /**
     * @return \ReflectionMethod[]
     */
    public function getMethods()
    {
        return $this->methods;
    }

    /**
     * @return string
     * @throws \ReflectionException
     */
    public function generate()
    {
        $template = $this->getTemplate();

        return str_replace(
            array(
                '{phpDoc}',
                '{namespace}',
                '{name}',
                '{methods}',
            ),
            array(
                'This file generate by InterfaceGenerator',
                $this->generateNamespace(),
                $this->getName(),
                $this->generateMethods()
            ),
            $template
        );
    }

    private function getTemplate()
    {
        return <<<INTRFC
<?php
/**
 * {phpDoc}
 */

{namespace}
interface {name}
{
    {methods}
}

INTRFC;
    }

    /**
     * @return string
     */
    private function generateNamespace()
    {
        return $this->getNamespace() ? "namespace {$this->getNamespace()};\n" : '';
    }

    /**
     * @return string
     * @throws \ReflectionException
     */
    private function generateMethods()
    {
        $methods = array();

        foreach ($this->getMethods() as $method) {
            $methods[] = $this->generateMethodString($method);
        }

        return implode("\n\n    ", $methods);
    }

    /**
     * @param \ReflectionMethod $method
     * @return string
     * @throws \ReflectionException
     */
    private function generateMethodString(\ReflectionMethod $method)
    {
        $parts = 'public ';
        if ($method->isStatic()) $parts .= 'static ';

        $parts .= 'function ' . $method->getName() . '(';
        $parts .= $this->generateMethodParams($method);
        $parts .= ')';

        if ($method->getReturnType()) $parts .= ' : ' . $method->getReturnType();

        $parts .= ';';

        return $parts;
    }

    /**
     * @param \ReflectionMethod $method
     * @return string
     * @throws \ReflectionException
     */
    private function generateMethodParams(\ReflectionMethod $method): string
    {
        $params = array();

        if ($method->getNumberOfParameters()) {
            foreach ($method->getParameters() as $param) {
                $item = $param->getType() . ' $' . $param->getName();
                if ($param->isOptional() && $param->isDefaultValueAvailable()) {
                    $item .= ' = ';
                    if (is_numeric($param->getDefaultValue()))
                        $item .= $param->getDefaultValue();
                    elseif (is_bool($param->getDefaultValue()))
                        $item .= (bool)$param->getDefaultValue() ? 'true' : 'false';
                    elseif (is_string($param->getDefaultValue()))
                        $item .= '\'' . addcslashes($param->getDefaultValue(), "'") . '\'';
                    elseif (is_array($param->getDefaultValue())) {
                        $item .= 'array()';
                    } elseif (is_null($param->getDefaultValue()) || empty($param->getDefaultValue()))
                        $item .= 'null';
                    else
                        $item .= $param->getDefaultValue();
                }
                $params[] = $item;
            }
        }

        return implode(', ', $params);
    }

}
