<?php
/**
 *
 */

namespace MyTest\Tests;

/**
 * Class AbstractClass
 * @package MyTest\Tests
 */
abstract class AbstractClass
{
    public function method1()
    {
        echo 'method1()';
    }

    /**
     * @param string $param1
     */
    abstract public function method2(string $param1);

    private function method3()
    {
        echo 'method3()';
    }
}