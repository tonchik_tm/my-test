<?php
/**
 *
 */

namespace MyTest\Tests;

/**
 * Class SimpleClass
 * @package MyTest\Tests
 */
class SimpleClass
{
    public function method1()
    {
        echo 'method1()';
    }

    /**
     * @param string $param1
     */
    public function method2(string $param1)
    {
        echo 'method2($param1)';
    }

    private function method3()
    {
        echo 'method3()';
    }
}