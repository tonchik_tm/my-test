<?php
/**
 *
 */

namespace MyTest\Tests;

/**
 * Class ChildClass
 * @package MyTest\Tests
 */
class ChildClass
{
    /**
     * @param string $param1
     */
    public function method2(string $param1)
    {
        $this->method4();
        echo 'method2($param1)';
    }

    private function method4()
    {
        echo 'method3()';
    }
}