<?php
/**
 *
 */

namespace MyTest\Tests;

/**
 * Interface InnerInterfaceClass
 * @package MyTest\Tests
 */
interface InnerInterfaceClass
{
    /**
     * InnerInterfaceClass constructor.
     */
    public function __construct();

    /**
     * @return mixed
     */
    public function method1();

    /**
     * @param string $param1
     */
    public function method2(string $param1);
}

/**
 * Class ImplementClass
 * @package MyTest\Tests
 */
class ImplementClass implements InnerInterfaceClass
{
    /**
     * ImplementClass constructor.
     */
    public function __construct()
    {
        /**/
    }

    /**
     * @return mixed|void
     */
    public function method1()
    {
        echo 'method1()';
    }

    /**
     * @param string $param1
     */
    public function method2(string $param1)
    {
        echo 'method2($param1)';
    }

    /**
     * @param int $param1
     * @return string
     */
    public function method5(int $param1) : string
    {
        echo 'method2($param1)';
        return 'success';
    }
}