<?php
/**
 *
 */

namespace MyTest\Tests;

/**
 * Interface
 * @package MyTest\Tests
 */
interface InterfaceClass
{
    public function method1();

    /**
     * @param string $param1
     */
    public function method2(string $param1);
}