<?php
/**
 *
 */

namespace MyTest\Tests;

trait TraitMethod1
{
    public function method1()
    {
        echo 'method1()';
    }
}

trait TraitMethod2
{
    /**
     * @param string $param1
     */
    public function method2(string $param1)
    {
        echo 'method2($param1)';
    }
}

/**
 * Class UseTraitClass
 * @package MyTest\Tests
 */
class UseTraitClass
{
    use TraitMethod1, TraitMethod2;

    /**
     * @param int $param1
     * @return string
     */
    public function method5(int $param1) : string
    {
        echo 'method2($param1)';
        return 'success';
    }
}