<?php
/**
 *
 */

namespace MyTest\Tests;

interface InterfaceAnotherClass
{
    public function method1();

    /**
     * @param string $param1
     */
    public function method2(string $param1);
}

/**
 * Class ImplementAnotherClass
 * @package MyTest\Tests
 */
class ImplementAnotherClass implements InterfaceAnotherClass
{
    public function method1()
    {
        echo 'method1()';
    }

    /**
     * @param string $param1
     */
    public function method2(string $param1)
    {
        echo 'method2($param1)';
    }

    /**
     * @param string $param1
     * @param int $param2
     * @return int
     */
    public function method7(string $param1, int $param2) : int
    {
        echo 'method7($param1, $param2)';

        return 1;
    }
}