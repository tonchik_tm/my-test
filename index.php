<?php
/**
 *
 */

require_once __DIR__ . '/vendor/autoload.php';

use TonchikTm\MyTest\Generator;
use TonchikTm\MyTest\ServiceUpload;

$interface = null;
$service = new ServiceUpload();
if ($service->upload('phpfile')) {
    $file = $service->getFile();
    try {
        $generator = new Generator($file);
        $interface = $generator->output();
    } catch (Exception $e) {
        $interface = $e->getMessage() . "\n\n";
    }
}
?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>

<div style="margin:15px 30px;padding: 15px 30px;border: 1px solid #999;">
    <form action="" method="post" enctype="multipart/form-data">
        <input type="file" name="phpfile" id="phpfile"> <input type="submit" value="upload">
    </form>
</div>

<?php if (!empty($interface)) : ?>

    <div style="margin:0;padding: 0 30px;">
        <p>Source File:</p>
        <textarea  style="width:100%;min-height:200px;border: 1px solid #999;"><?= $service->getFileContent(); ?></textarea>
    </div>

    <div style="margin:0;padding: 0 30px;">
        <p>Interface:</p>
        <textarea  style="width:100%;min-height:200px;border: 1px solid #999;"><?= $interface; ?></textarea>
    </div>
<?php endif; ?>
</body>
</html>
